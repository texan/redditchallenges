# Challenge #296 [Intermediate] Intersecting Area Of Overlapping Rectangles
# http://reddit.com/r/dailyprogrammer/comments/5jpt8v/20161222_challenge_296_intermediate_intersecting/

import unittest

		
class Point:
	def __init__(self, x, y):
		self.x = x
		self.y = y
		

class Rectangle:
	def __init__(self, corner, opposingCorner):
		self.corner = corner
		self.opposingCorner = opposingCorner
	
	def GetXMin(self):
		return min(self.corner.x, self.opposingCorner.x)
	
	def GetXMax(self):
		return max(self.corner.x, self.opposingCorner.x)
	
	def GetYMin(self):
		return min(self.corner.y, self.opposingCorner.y)
	
	def GetYMax(self):
		return max(self.corner.y, self.opposingCorner.y)
		
		
class Diffs:
	def __init__(self, recList):
		self.recList = recList
		
	def GetGreatestXMin(self):
		minXValue = self.recList[0].GetXMin()
		
		for rec in self.recList:
			if (rec.GetXMin() > minXValue):
				minXValue = rec.GetXMin()
		return minXValue
		
	def GetSmallestXMax(self):
		maxXValue = self.recList[0].GetXMax()
		
		for rec in self.recList:
			if (rec.GetXMax() < maxXValue):
				maxXValue = rec.GetXMax()
				
		return maxXValue
		
	def GetGreatestYMin(self):
		minYValue = self.recList[0].GetYMin()
		
		for rec in self.recList:
			if (rec.GetYMin() > minYValue):
				minYValue = rec.GetYMin()
				
		return minYValue
		
	def GetSmallestYMax(self):
		maxYValue = self.recList[0].GetYMax()
		
		for rec in self.recList:
			if (rec.GetYMax() < maxYValue):
				maxYValue = rec.GetYMax()
				
		return maxYValue
	
	def GetXRange(self):
		xRange = self.GetSmallestXMax() - self.GetGreatestXMin()
		if xRange <= 0:
			return 0
			
		return xRange
		
	def GetYRange(self):
		yRange = self.GetSmallestYMax() - self.GetGreatestYMin()
		if yRange <= 0:
			return 0
			
		return yRange
	
	def GetArea(self):
		return self.GetXRange() * self.GetYRange()


# ~~~ What Did We Test in the Class Tonight, Craig? ~~~ #

class testRectangle(unittest.TestCase):
	
	def setUp(self):
		self.rec = Rectangle(Point(0, 0), Point(2, 2))
	
	def testGetXMin(self):
		result = self.rec.GetXMin()
		self.assertEqual(result, 0)
	
	def testGetXMax(self):
		result = self.rec.GetXMax()
		self.assertEqual(result, 2)
		
	def testGetYMin(self):
		result = self.rec.GetYMin()
		self.assertEqual(result, 0)
	
	def testGetYMax(self):
		result = self.rec.GetYMax()
		self.assertEqual(result, 2)
		
	def tearDown(self):
		self.rec = None
		
				
class testDiffs(unittest.TestCase):
	
	def setUp(self):
		rec1 = Rectangle(Point(0, 0), Point(2, 2))
		rec2 = Rectangle(Point(1, 1), Point(3, 3))
		rec3 = Rectangle(Point(5.8, 5), Point(-3, 2.4))
		self.diffs = Diffs(list([rec1, rec2, rec3]))
	
	def test_GetSmallestXMax(self):
		result = self.diffs.GetSmallestXMax()
		self.assertEqual(result, 2.0)
		
	def test_GetGreatestXMin(self):
		result = self.diffs.GetGreatestXMin()
		self.assertEqual(result, 1.0)
		
	def test_GetSmallestYMax(self):
		result = self.diffs.GetSmallestYMax()
		self.assertEqual(result, 2.0)
		
	def test_GetGreatestYMin(self):
		result = self.diffs.GetGreatestYMin()
		self.assertEqual(result, 2.4)
		
	def test_GetXRange(self):
		result = self.diffs.GetXRange()
		self.assertEqual(result, 1.0)

	def test_GetYRange(self):
		result = self.diffs.GetYRange()
		self.assertEqual(result, 0.0)
	
	def test_GetArea(self):
		result = self.diffs.GetArea()
		self.assertEqual(result, 0.0)
		
	def tearDown(self):
		self.diffs = None
	

class testChallenges(unittest.TestCase):
	
	def testChallengeArea(self):
		rec1 = Rectangle(Point(0, 0), Point(2, 2))
		rec2 = Rectangle(Point(1, 1), Point(3, 3))
		diffs = Diffs(list([rec1, rec2]))
		result = diffs.GetArea()
		self.assertEqual(result, 1.0)
		
	def testChallengeArea2(self):
		rec1 = Rectangle(Point(-3.5, 4), Point(1, 1))
		rec2 = Rectangle(Point(1, 3.5), Point(-2.5, -1))
		diffs = Diffs(list([rec1, rec2]))
		result = diffs.GetArea()
		self.assertEqual(result, 8.75)
	
	def testChallengeArea3(self):
		rec1 = Rectangle(Point(-4, 4), Point(-0.5, 2))
		rec2 = Rectangle(Point(0.5, 1), Point(3.5, 3))
		diffs = Diffs(list([rec1, rec2]))
		result = diffs.GetArea()
		self.assertEqual(result, 0.0)
		
	def testBonusArea(self):
		rec1 = Rectangle(Point(-3, 0), Point(1.8, 4))
		rec2 = Rectangle(Point(1, 1), Point(-2.5, 3.6))
		rec3 = Rectangle(Point(-4.1, 5.75), Point(0.5, 2))
		rec4 = Rectangle(Point(-1.0, 4.6), Point(-2.9, -0.8))
		diffs = Diffs(list([rec1, rec2, rec3, rec4]))
		result = diffs.GetArea()
		self.assertAlmostEqual(result, 2.4)
		
		
unittest.main()
