# [2017-10-09] Challenge #335 [Easy] Consecutive Distance Rating
# http://reddit.com/r/dailyprogrammer/comments/759fha/20171009_challenge_335_easy_consecutive_distance/

import unittest
import console


class DistanceRating:
	
	def __init__(self, seqNum, seqLen, inputLists, gap=1):
		self.seqNum = seqNum
		self.seqLen = seqLen
		self.inputLists = inputLists
		self.gap = gap
	
	def PrintResults(self, test=False):
		results = list()
		
		for index in range(self.seqNum):
			result = self.CalculateRating(index)
			results.append(result)
			if not test:
				print(result)
			
		if test:
			return results
	
	def CalculateRating(self, index=0):
		if self.seqNum == 1:
			return self.GetRating(self.inputLists, self.seqLen, self.gap)
		elif self.seqNum > 1:
			return self.GetRating(self.inputLists[index], self.seqLen, self.gap)
	
	@staticmethod
	def GetRating(inputList, length, gap=1):
		
		# The sum of the distances between consecutive (or gapped) integers
		rating = 0
		
		# Sort the input list to check for consectutives
		sortedList = sorted(inputList)
		
		# Turns inputList.Index into inputIndex to reduce line length on calls
		def inputIndex(x):
			return inputList.index(x)
		
		if gap > 0:
			for i in range(length):
				if (sortedList[i] - gap) in inputList:
					rating += abs(inputIndex(sortedList[i]) - inputIndex(sortedList[i] - gap))
		else:
			raise ValueError("Gap must be greater than zero.")

		return rating
		
	@staticmethod
	def Clear():
		console.clear()

# Build out the lists
challengeList1 = list([76, 74, 45, 48, 13])
challengeList1 += list([75, 16, 14, 79, 58])
challengeList1 += list([78, 82, 46, 89, 81])
challengeList1 += list([88, 27, 64, 21, 63])

challengeList2 = list([37, 35, 88, 57, 55])
challengeList2 += list([29, 96, 11, 25, 42])
challengeList2 += list([24, 81, 82, 58, 15])
challengeList2 += list([2, 3, 41, 43, 36])

challengeList3 = list([54, 64, 52, 39, 36])
challengeList3 += list([98, 32, 87, 95, 12])
challengeList3 += list([40, 79, 41, 13, 53])
challengeList3 += list([35, 48, 42, 33, 75])

challengeList4 = list([21, 87, 89, 26, 85])
challengeList4 += list([59, 54, 2, 24, 25])
challengeList4 += list([41, 46, 88, 60, 63])
challengeList4 += list([23, 91, 62, 61, 6])

challengeList5 = list([94, 66, 18, 57, 58])
challengeList5 += list([54, 93, 53, 19, 16])
challengeList5 += list([55, 22, 51, 8, 67])
challengeList5 += list([20, 17, 56, 21, 59])

challengeList6 = list([6, 19, 45, 46, 7])
challengeList6 += list([70, 36, 2, 56, 47])
challengeList6 += list([33, 75, 94, 50, 34])
challengeList6 += list([35, 73, 72, 39, 5])

challengeList = list()
challengeList.append(challengeList1)
challengeList.append(challengeList2)
challengeList.append(challengeList3)
challengeList.append(challengeList4)
challengeList.append(challengeList5)
challengeList.append(challengeList6)

challenge = DistanceRating(6, 20, challengeList, 1)
challenge.Clear()
challenge.PrintResults()


# ~~~ What Did We Test in the Class Tonight, Craig? ~~~ #

class testDistanceRating(unittest.TestCase):
	
	def testGetRatingShouldReturn7(self):
		newList = list([5, 8, 4, 7, 9])
		result = DistanceRating.GetRating(newList, 5)
		self.assertEqual(result, 7)

	def testGetRatingShouldReturn19(self):
		newList = list([5, 8, 63, 4, 7, 19, 9, 64, 20])
		result = DistanceRating.GetRating(newList, 9)
		self.assertEqual(result, 19)
		
	def testGetRatingGap2ShouldBe14(self):
		newList = list([5, 8, 63, 4, 7, 19, 9, 65, 21])
		result = DistanceRating.GetRating(newList, 9, 2)
		self.assertEqual(result, 14)
	
	def testGetRatingGap15ShouldBe17(self):
		newList = list([24, 34, 63, 4, 39, 19, 54, 78])
		result = DistanceRating.GetRating(newList, 8, 15)
		self.assertEqual(result, 17)

	def testCalculateRatingShouldBe9(self):
		test = DistanceRating(1, 6, list([16, 5, 8, 4, 15, 9]))
		result = test.CalculateRating()
		self.assertEqual(result, 9)
		
	def testPrintResultsTwoLists(self):
		test = DistanceRating(2, 4, list([list([4, 6, 7, 5]), list([3, 8, 1, 4])]))
		result = test.PrintResults(True)
		self.assertEqual(result, list([6, 3]))
	
	def testGetRatingValueError(self):
		test = DistanceRating(1, 4, list([62, 16, 15, 9]), -2)
		self.assertRaises(ValueError, test.CalculateRating)

# Run Unit Tests, Always
print()
unittest.main()
