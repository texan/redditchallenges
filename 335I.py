# [2017-10-11] Challenge #335 [Intermediate] Scoring a Cribbage Hand
# http://reddit.com/r/dailyprogrammer/comments/75p1cs/20171011_challenge_335_intermediate_scoring_a/

import unittest
from itertools import combinations

# Switch Statements?


class Card:
	def __init__(self, value, suit):
		self.value = value
		self.suit = suit

	def GetValue(self):
		if self.value[0] == 'A':
			return 1
		if self.value[0] == '1':
			return 10
		try:
			return int(self.value)
		except:
			return 10
		
	def GetOrder(self):
		if self.value[0] == 'A':
			return 1
		if self.value[0] == '1':
			return 10
		if self.value[0] == 'J':
			return 11
		if self.value[0] == 'Q':
			return 12
		if self.value[0] == 'K':
			return 13
		return int(self.value)
		

class Hand:
	def __init__(self, hand):
		self.hand = hand.split(",")
		self.HandToCards()
		self.faceUp = self.hand[4]
		self.points = 0
		self.scoring = ""
		self.CalculatePoints()
	
	def HandToCards(self):
		for i in range(5):
			card = list(self.hand[i])
			self.hand[i] = Card(card[:-1][0], card[-1:][0])
		
	def CalculatePoints(self, output=False):
		previous = 0
		fifteens = 0
		pairs = 0
		runs = 0
		flush = 0
		nob = False
		self.Reset()

		self.Fifteens()
		if previous != self.points:
			fifteens = int(self.points / 2)
			previous = self.points
		
		self.Pairs()
		if previous != self.points:
			pairs = int((self.points - previous) / 2)
			previous = self.points
		
		self.Runs()
		if previous != self.points:
			runs = self.points - previous
			previous = self.points
		
		self.Flush()
		if previous != self.points:
			flush = self.points - previous
			previous = self.points
		
		self.Nob()
		if previous != self.points:
			nob = 1
		
		self.Scoring(fifteens, pairs, runs, flush, nob)
		if output:
			self.PrintScoring()
		
		return self.points
	
	def Fifteens(self):
		for i in range(2, 6):
			for cardCombo in combinations(self.hand, i):
				comboTotal = 0
				for card in cardCombo:
					comboTotal += card.GetValue()
				if comboTotal == 15:
					self.points += 2
		return self.points
		
	def Pairs(self):
		for cardCombo in combinations(self.hand, 2):
			if cardCombo[0].value == cardCombo[1].value:
				self.points += 2
		return self.points
		
	def Runs(self):
		for i in range(3, 5):
			for cardCombo in combinations(self.hand, i):
				order = list()
				for card in cardCombo:
					order.append(card.GetOrder())
				if max(order) - min(order) == i - 1 and len(set(order)) == i:
					if i == 3:
						self.points += 3
					if i == 4:
						self.points -= 2
		return self.points
	
	def Flush(self):
		suit = self.hand[0].suit
		for card in self.hand[:-1]:
			if card.suit != suit:
				return 0
		if suit == self.faceUp.suit:
			self.points += 5
		else:
			self.points += 4
		return self.points
		
	def Nob(self):
		for card in self.hand[:-1]:
			if card.value == 'J' and card.suit == self.faceUp.suit:
				self.points += 1
		return self.points
		
	def Reset(self):
		self.points = 0
		
	def Scoring(self, fifteens, pairs, runs, flush, nob):
		started = False
		
		if fifteens > 0:
			if fifteens == 1:
				self.scoring += str(fifteens) + " fifteen"
			else:
				self.scoring += str(fifteens) + " fifteens"
			started = True
		
		if pairs > 0 and started:
			self.scoring += ", "
		elif pairs > 0 and not started:
			started = True
		if pairs == 1:
			self.scoring += "1 pair"
		if pairs == 2:
			self.scoring += "2 pairs"
		if pairs == 3:
			self.scoring += "a three-of-a-kind"
		if pairs == 4:
			self.scoring += "a three-of-a-kind and a pair"
		if pairs == 6:
			self.scoring += "a four-of-a-kind"

		if runs > 0 and started:
			self.scoring += ", "
		elif runs > 0 and not started:
			started = True		
		if runs == 3:
			self.scoring += "3-card run"
		if runs == 4:
			self.scoring += "4-card run"
		if runs == 5:
			self.scoring += "5-card run"
		if runs == 6:
			self.scoring += "3-card runs"
		if runs == 8:
			self.scoring += "4-card runs"
		
		if flush > 0 and started:
			self.scoring += ", a "
		elif flush > 0 and not started:
			self.scoring += "A "			
			started = True
		if flush == 4:
			self.scoring += "4-card flush"
		if flush == 5:
			self.scoring += "5-card flush"
			
		return list([fifteens, pairs, runs, flush, nob])
		
	def PrintScoring(self):
		print(self.scoring)


class testHand(unittest.TestCase):
	
	# setUp for List testing
	# This will require the print to be outside the constructor
	# def setUp(self):
		# cards = '5D,QS,JC,KH,AC'
		# hand = Hand(cards)
		# hand.Reset()
	
	# Dugga Dugga Challenges
	# 3 fifteens, run of 3, nob
	def testGetRatingShouldReturn10(self):
		newList = '5D,QS,JC,KH,AC'
		result = Hand(newList).points
		self.assertEqual(result, 10)
		
	# 2 fifteens, run of 3
	def testGetRatingShouldReturn7(self):
		newList = '8C,AD,10C,6H,7S'
		result = Hand(newList).points
		self.assertEqual(result, 7)
	
	# 2 fifteens
	def testGetRatingShouldReturn4(self):
		newList = 'AC,6D,5C,10C,8C'
		result = Hand(newList).points
		self.assertEqual(result, 4)
	
	# Flush
	def testFlushFiveCardsShouldReturn5(self):
		newList = 'AC,6C,5C,10C,8C'
		result = Hand(newList)
		result.Reset()
		result.Flush()
		self.assertEqual(result.points, 5)
		
	def testFlushFourCardsShouldReturn4(self):
		newList2 = 'AC,6C,5C,10C,8D'
		result3 = Hand(newList2)
		result3.Reset()
		result3.Flush()
		self.assertEqual(result3.points, 4)
	
	def testFlushThreeCardsWithFaceUpShouldReturn0(self):
		newList3 = 'AC,6H,5C,10C,8C'
		result2 = Hand(newList3)
		result2.Reset()
		result2.Flush()
		self.assertEqual(result2.points, 0)
			
	# Nob
	def testNobShouldReturn1(self):
		newList = 'KC,JD,4H,10C,10D'
		result = Hand(newList)
		result.Reset()
		result.Nob()
		self.assertEqual(result.points, 1)
		
	def testNobShouldReturn0(self):
		newList = 'KC,4D,4H,10C,JD'
		result = Hand(newList)
		result.Reset()
		result.Nob()
		self.assertEqual(result.points, 0)
		
	# Fifteens
	def testCount3ShouldReturn6(self):
		newList = '2C,3D,JH,QC,KD'
		result = Hand(newList)
		result.Reset()
		result.Fifteens()
		self.assertEqual(result.points, 6)
		
	def testCount4ShouldReturn4(self):
		newList = '2C,AD,4H,8C,2D'
		result = Hand(newList)
		result.Reset()
		result.Fifteens()
		self.assertEqual(result.points, 4)
			
	def testCount5ShouldReturn2(self):
		newList = '2C,AD,4H,3C,5D'
		result = Hand(newList)
		result.Reset()
		result.Fifteens()
		self.assertEqual(result.points, 2)
		
	def testFifteensShouldReturn6(self):
		newList = '2C,6C,7H,8S,AC'
		result = Hand(newList)
		result.Reset()
		result.Fifteens()
		self.assertEqual(result.points, 6)
		
	# Pairs
	def testPairsShouldReturn8(self):
		newList = '2C,8C,8H,8S,2C'
		result = Hand(newList)
		result.Reset()
		result.Pairs()
		self.assertEqual(result.points, 8)

	# Runs
	def testRunsShouldReturn8(self):
		newList = '2C,3C,4H,4S,5C'
		result = Hand(newList)
		result.Reset()
		result.Runs()
		self.assertEqual(result.points, 8)
		
	def testRunsShouldReturn5(self):
		newList = 'AC,5C,3H,4S,2C'
		result = Hand(newList)
		result.Reset()
		result.Runs()
		self.assertEqual(result.points, 5)
		
	def testSteveShouldReturn13(self):
		newList = 'AD,AC,6D,8H,7D'
		result = Hand(newList).points
		self.assertEqual(result, 13)
		
		
unittest.main()
