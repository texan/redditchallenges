﻿// https://www.reddit.com/r/dailyprogrammer/comments/3ufwyf/20151127_challenge_242_hard_start_to_rummikub/

using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C242Hard
{
    public enum Colors { Black, Purple, Red, Yellow }

    public class Tile
    {
        public short Number { get; private set; }
        public Colors Color { get; private set; }
        public short FirstSecond { get; private set; }

        public Tile(short number, Colors color, short firstSecond)
        {
            Number = number;
            Color = color;
            FirstSecond = firstSecond;
        }

        public override string ToString()
        {
            // Jokers
            if (Number == 14)
                return string.Format($"{Color.ToString().Substring(0, 1)} Joker");

            return string.Format($"{Color.ToString().Substring(0, 1)}{Number}");
        }
    }

    public class TileBag
    {
        ArrayList tileBag = new ArrayList();

        public TileBag()
        {
            for (short i = 1; i <= 13; i++)
                for (short j = 1; j <= 4; j++)
                    for (short k = 1; k <= 2; k++)
                        tileBag.Add(new Tile(i, (Colors)j, k));

            // Jokers
            tileBag.Add(new Tile(14, (Colors)1, 1));
            tileBag.Add(new Tile(14, (Colors)3, 1));
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            
        }
    }
}
